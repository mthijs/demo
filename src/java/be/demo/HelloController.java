/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.demo;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author MTJAH17
 */
@Named
@RequestScoped
public class HelloController implements Serializable{
    
    private String name;
    
    @Inject
    private GreetingService greetingService;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getGreetingFromBusinessLayer(){
        return "greeting: "+greetingService.sayHello();
    }
}
